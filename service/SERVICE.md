# Service

The service consists of the Domain and the Application layer, which in this app I've confusingly called `domain.ts` and `main.ts`.

## Domain

The domain is the pure ts types representing the mental model of our users/stakeholders and should be 1-1 with their language and "simulate" the domain, as it were. This is all stolen from DDD concepts from Eric Evans and some good talks/books about functional domain modeling by Scott Wlaschin.

The `domain.ts` file is just a bunch of types plus some behaviors for interacting with those types.

## Application

`main.js` houses and exports the core application level behaviors of the service, which in this case are `fetchCourses`, `register`, and `unregister`. They are independent of any specific technology implementation, so they can pop into any controller (in this case gql, but could just as easily be rest, grpc, a kinesis consumer, a direct function call from nextjs's `getServerSideProps`, etc.)

Additionally, since I love ports and adapters, these behaviors rely on interfaces/abstractions instead of concrete implementations. Specifically, they don't care how the `CourseRepo` interface is implemented, be it Postgres, In Memory, Hard Coded, etc. As long as it's implemented to the contract you can dependency inject it. This keeps dependencies pointing inward instead of coupling together application logic, domain logic, and technical details like choice of db. IMO, it's the only* way to design a backend (*for business domains, maybe not for querying, simple CRUD apps like this should have been, etc.). This also makes it really easy to test in isolation.
