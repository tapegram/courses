import { pipe } from "fp-ts/lib/pipeable";
import * as TE from "fp-ts/lib/TaskEither";
import * as D from "./domain";

export const fetchCourses = (
  courseRepo: D.CourseRepo
): TE.TaskEither<D.Failure, D.Course[]> => courseRepo.fetchAll();

export const register = (
  courseRepo: D.CourseRepo,
  studentId: D.StudentId,
  sectionId: D.SectionId
): TE.TaskEither<D.Failure, D.Course> =>
  pipe(
    courseRepo.fetchAll(),
    TE.chain((courses: D.Course[]) =>
      findCourseBySectionId(courses, sectionId)
    ),
    TE.chain((course: D.Course) =>
      addStudentToCourse(course, sectionId, studentId)
    ),
    TE.chain(courseRepo.update)
  );

export const unregister = (
  courseRepo: D.CourseRepo,
  studentId: D.StudentId,
  sectionId: D.SectionId
): TE.TaskEither<D.Failure, D.Course> =>
  pipe(
    courseRepo.fetchAll(),
    TE.chain((courses: D.Course[]) =>
      findCourseBySectionId(courses, sectionId)
    ),
    TE.chain((course: D.Course) =>
      unregisterFromCourse(course, sectionId, studentId)
    ),
    TE.chain(courseRepo.update)
  );

// Shims to hide some of the grossness of converting datatypes to fit in a pipe
const findCourseBySectionId = (courses: D.Course[], sectionId: string) =>
  TE.fromOption<D.Failure>(() =>
    D.toFailure("Could not find course with provided section id")
  )(D.findCourseBySectionId(courses, sectionId));

const addStudentToCourse = (
  course: D.Course,
  sectionId: string,
  studentId: string
) => TE.fromEither(D.addStudentToCourse(course, sectionId, studentId));

const unregisterFromCourse = (
  course: D.Course,
  sectionId: string,
  studentId: string
) => TE.fromEither(D.unregisterFromCourse(course, sectionId, studentId));
