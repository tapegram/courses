import { Either, right, left, map, fromOption } from "fp-ts/lib/Either";
import { Option, fromNullable, isSome } from "fp-ts/Option";
import { chain } from "fp-ts/lib/Either";
import { pipe } from "fp-ts/lib/pipeable";
import * as TE from "fp-ts/lib/TaskEither";

export type StudentId = string;

export type CourseId = string;
export type CourseName = string;
export type CourseDescription = string;
export type Course = {
  id: CourseId;
  name: CourseName;
  description: CourseDescription;
  sessions: Session[];
  sections: Section[];
};
export type SessionId = string;
export type SessionNumber = string;
export type SessionDescription = string;
export type SessionName = string;
export type Session = {
  id: SessionId;
  sessionNumber: SessionNumber;
  name: SessionName;
  description: SessionDescription;
};

export type SectionId = string;
export type NickName = string;
export type Date = {
  day: number;
  month: number;
  year: number;
};
export type Section = {
  id: SectionId;
  nickName: NickName;
  start: Date;
  students: Set<StudentId>;
};

export type Failure = {
  msg: string;
};

export const toFailure = (msg: string) => {
  return {
    msg,
  };
};

export type CourseRepo = {
  fetchAll: () => TE.TaskEither<Failure, Course[]>;
  update: (course: Course) => TE.TaskEither<Failure, Course>;
};

export const addStudentToCourse = (
  course: Course,
  sectionId: SectionId,
  studentId: StudentId
): Either<Failure, Course> => {
  return pipe(
    fromOption(() => toFailure("Could not find section"))(
      findSection(course, sectionId)
    ),
    chain((section) =>
      hasOpenSeats(section)
        ? right(section)
        : left(toFailure("Section is full"))
    ),
    map(() => {
      return {
        ...course,
        sections: course.sections.map((it) =>
          it.id == sectionId
            ? {
                ...it,
                students: it.students.add(studentId),
              }
            : it
        ),
      };
    })
  );
};

export const unregisterFromCourse = (
  course: Course,
  sectionId: SectionId,
  studentId: StudentId
): Either<Failure, Course> => {
  return pipe(
    fromOption(() => toFailure("Could not find section"))(
      findSection(course, sectionId)
    ),
    map((section) => unregisterStudentFromSection(section, studentId)),
    map((section) => {
      return {
        ...course,
        sections: course.sections.map((it) =>
          it.id == sectionId ? section : it
        ),
      };
    })
  );
};

export const findCourseBySectionId = (
  courses: Course[],
  sectionId: SectionId
): Option<Course> =>
  fromNullable(courses.find((course) => hasSection(course, sectionId)));

const unregisterStudentFromSection = (
  section: Section,
  studentId: StudentId
): Section => {
  return {
    ...section,
    students: new Set(remove(section.students, studentId)),
  };
};

const remove = <T>(iterable: Iterable<T>, elem: T) =>
  [...iterable].filter((e) => e != elem);

const hasOpenSeats = (section: Section): Boolean => section.students.size < 10;

const hasSection = (course: Course, sectionId: SectionId): Boolean =>
  isSome(findSection(course, sectionId));

export const findSection = (
  course: Course,
  sectionId: SectionId
): Option<Section> =>
  fromNullable(course.sections.find((it) => it.id == sectionId));
