import { FetchCoursesDocument } from "../../lib/courses.graphql";
import { initializeApollo } from "../../lib/apollo";
import nookies from "nookies";
import { toRegistrations } from "../../lib/shared";
import { Registration } from "../../lib/shared";
import StudentProfile from "../../components/studentProfile";
import Authed from "../../components/authed";

type Props = {
  isAuthed: Boolean;
  studentId: string | null;
  registrations: Registration[];
};

const Index = (props: Props) => {
  return (
    <Authed isAuthed={props.isAuthed}>
      <StudentProfile
        studentId={props.studentId!}
        registrations={props.registrations}
      />
    </Authed>
  );
};

export async function getServerSideProps(context) {
  const apolloClient = initializeApollo();

  const { data } = await apolloClient.query({
    query: FetchCoursesDocument,
  });

  const courses = data!.fetchCourses;

  const cookies = nookies.get(context);
  const studentId = cookies.course_student_id;
  const props: Props = {
    isAuthed: !!studentId,
    registrations: toRegistrations(studentId, courses),
    studentId: studentId || null,
  };

  return { props };
}

export default Index;
