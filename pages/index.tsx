const Index = () => (
  <>
    <h2>Welcome to the Courses website home page!</h2>
    <p>
      Please feel free to poke around but not too much (don't want to expose any
      bugs!)
    </p>
  </>
);

export default Index;
