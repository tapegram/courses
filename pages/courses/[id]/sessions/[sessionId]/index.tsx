import nookies from "nookies";
import * as T from "../../../../../lib/type-defs.graphqls";
import { initializeApollo } from "../../../../../lib/apollo";
import { FetchCoursesDocument } from "../../../../../lib/courses.graphql";
import Authed from "../../../../../components/authed";
import Error from "../../../../../components/error";
import Session from "../../../../../components/session";
import {
  getSession,
  getCourse,
  isRegisteredForCourse,
} from "../../../../../lib/shared";
import { Option, chain, fold, none } from "fp-ts/Option";
import { pipe } from "fp-ts/lib/pipeable";

type Props = {
  session: Option<T.Session>;
  isAuthed: Boolean;
  canViewSession: Boolean;
};

const Index = (props: Props) => (
  <Authed isAuthed={props.isAuthed}>
    {props.canViewSession ? (
      pipe(
        props.session,
        fold(
          () => <Error message="Session not found!" />,
          (session: T.Session) => <Session session={session} />
        )
      )
    ) : (
      <Error message="You can't view this page unless you are enrolled" />
    )}
  </Authed>
);

export async function getServerSideProps(context) {
  const cookies = nookies.get(context);
  const studentId = cookies.course_student_id;

  const apolloClient = initializeApollo();
  const { data } = await apolloClient.query({
    query: FetchCoursesDocument,
  });

  const { id, sessionId } = context.params;
  return { props: toProps(studentId, id, sessionId, data.fetchCourses) };
}

const toProps = (
  studentId: string,
  courseId: string,
  sessionId: string,
  courses: T.Course[]
): Props => {
  const course: Option<T.Course> = pipe(courses, (courses) =>
    getCourse(courseId, courses)
  );
  const session: Option<T.Session> = chain((course: T.Course) =>
    getSession(course, sessionId)
  )(course);
  const canViewSession: Boolean = pipe(
    course,
    fold(
      () => false,
      (course: T.Course) => isRegisteredForCourse(studentId, course)
    )
  );

  return {
    isAuthed: !!studentId,
    session: canViewSession && !!studentId ? session : none, // Don't expose session to the client if they aren't allowed to see it
    canViewSession: canViewSession,
  };
};

export default Index;
