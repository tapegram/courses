import { FetchCoursesDocument } from "../../../lib/courses.graphql";
import { initializeApollo } from "../../../lib/apollo";
import nookies from "nookies";
import Course from "../../../components/course";
import Error from "../../../components/error";
import Authed from "../../../components/authed";
import { Course as TCourse } from "../../../lib/type-defs.graphqls";

type Props = {
  isAuthed: Boolean;
  course: TCourse | null;
  studentId: string | null;
};

const Index = (props: Props) => (
  <Authed isAuthed={props.isAuthed}>
    {!!props.course ? (
      <Course course={props.course} studentId={props.studentId!} />
    ) : (
      <Error message="Course not found" />
    )}
  </Authed>
);

export async function getServerSideProps(context) {
  const apolloClient = initializeApollo();

  const { data } = await apolloClient.query({
    query: FetchCoursesDocument,
  });

  const courseId = context.params.id;
  const courses: TCourse[] = data!.fetchCourses;
  const course = courses.find((it) => it.id == courseId);

  const cookies = nookies.get(context);
  const studentId = cookies.course_student_id;

  const props: Props = {
    isAuthed: !!studentId,
    course: course || null,
    studentId: studentId || null,
  };
  return { props };
}

export default Index;
