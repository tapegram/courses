import { FetchCoursesDocument } from "../../lib/courses.graphql";
import { initializeApollo } from "../../lib/apollo";
import Courses from "../../components/courses";
import Authed from "../../components/authed";
import SeedDB from "../../components/seeddb";
import { Course } from "../../lib/type-defs.graphqls";
import nookies from "nookies";

type Props = {
  isAuthed: Boolean;
  courses: Course[];
};

const Index = (props: Props) => (
  <Authed isAuthed={props.isAuthed}>
    <SeedDB />
    <Courses courses={props.courses} />
  </Authed>
);

export async function getServerSideProps(context) {
  const apolloClient = initializeApollo();

  const { data } = await apolloClient.query({
    query: FetchCoursesDocument,
  });

  const cookies = nookies.get(context);
  const studentId = cookies.course_student_id;

  const props: Props = {
    isAuthed: !!studentId,
    courses: data!.fetchCourses,
  };
  return { props };
}

export default Index;
