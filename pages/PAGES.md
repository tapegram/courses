# Pages

I chose nextjs for this not because it was the best fit (whatever the closest js thing is to Rails would have probably been lightning quick to set up a simple CRUD app) but because it seemed like a good excuse to learn nextjs.

But because I did I got this could page directory structure for free where routes are based on the folder structure.

Additionally, I decided to mostly use server side rendering because I thought it was cool and the user experience is a little bit better than the flashing dynamic loading (though that can be fixed with some extra FE work that I don't know how to do). It's also a little faster since it avoids some round trips. I also technically didn't have to use graphql to fetch the server side data since I could have just called the service methods directly, but that might not work in the future when we move the API to a separately deployable repo.
