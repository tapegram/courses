import { parseCookies } from "nookies";
import Login from "../../components/login";

type Props = {
  studentId: string | null;
  fromRedirect: Boolean;
};

const Index = (props: Props) => {
  return (
    <Login studentId={props.studentId} fromRedirect={props.fromRedirect} />
  );
};

export async function getServerSideProps(context) {
  const cookies = parseCookies(context);
  const props: Props = {
    studentId: cookies.course_student_id || null,
    fromRedirect: (context.query && context.query.fromRedirect) || false,
  };
  return { props };
}

export default Index;
