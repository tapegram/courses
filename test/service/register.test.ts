import * as E from "fp-ts/lib/Either";
import { register } from "../../service/main";
import { toFailure } from "../../service/domain";
import { toInMemoryRepo } from "../../data/inMemoryRepo";
import {
  toCourse,
  toDefaultSessions,
  toEmptySection,
  toFailingRepo,
  toFullSection,
  toInMemoryRepoThatFailsOnUpdate,
  toSectionWithOneSeatOpen,
} from "./shared";

const toFullStackDevCourseWithEmptySection = () =>
  toCourse(
    "1",
    "Full Stack Dev",
    "Learn Full Stack Dev!",
    toDefaultSessions("1"),
    [toEmptySection("1")]
  );

const toFullStackDevCourseWithFullSection = () =>
  toCourse(
    "1",
    "Full Stack Dev",
    "Learn Full Stack Dev!",
    toDefaultSessions("1"),
    [toFullSection("1")]
  );

const toFullStackDevCourseWithNearlyFullSection = () =>
  toCourse(
    "1",
    "Full Stack Dev",
    "Learn Full Stack Dev!",
    toDefaultSessions("1"),
    [toSectionWithOneSeatOpen("1")]
  );

describe("registerForSection", () => {
  it("adds student to section if spot is free", async () => {
    const repo = toInMemoryRepo([toFullStackDevCourseWithEmptySection()]);
    expect(await register(repo, "student123", "1-1")()).toEqual(
      E.right({
        ...toFullStackDevCourseWithEmptySection(),
        sections: [
          {
            id: "1-1",
            nickName: "The A Team",
            start: {
              day: 1,
              month: 1,
              year: 2020,
            },
            students: new Set(["student123"]),
          },
        ],
      })
    );

    expect(await repo.fetchAll()()).toEqual(
      E.right([
        {
          ...toFullStackDevCourseWithEmptySection(),
          sections: [
            {
              id: "1-1",
              nickName: "The A Team",
              start: {
                day: 1,
                month: 1,
                year: 2020,
              },
              students: new Set(["student123"]),
            },
          ],
        },
      ])
    );
  });

  it("does nothing if student is already registered", async () => {
    const repo = toInMemoryRepo([toFullStackDevCourseWithEmptySection()]);
    expect(await register(repo, "student123", "1-1")()).toEqual(
      E.right({
        ...toFullStackDevCourseWithEmptySection(),
        sections: [
          {
            id: "1-1",
            nickName: "The A Team",
            start: {
              day: 1,
              month: 1,
              year: 2020,
            },
            students: new Set(["student123"]),
          },
        ],
      })
    );

    expect(await repo.fetchAll()()).toEqual(
      E.right([
        {
          ...toFullStackDevCourseWithEmptySection(),
          sections: [
            {
              id: "1-1",
              nickName: "The A Team",
              start: {
                day: 1,
                month: 1,
                year: 2020,
              },
              students: new Set(["student123"]),
            },
          ],
        },
      ])
    );

    expect(await register(repo, "student123", "1-1")()).toEqual(
      E.right({
        ...toFullStackDevCourseWithEmptySection(),
        sections: [
          {
            id: "1-1",
            nickName: "The A Team",
            start: {
              day: 1,
              month: 1,
              year: 2020,
            },
            students: new Set(["student123"]),
          },
        ],
      })
    );

    expect(await repo.fetchAll()()).toEqual(
      E.right([
        {
          ...toFullStackDevCourseWithEmptySection(),
          sections: [
            {
              id: "1-1",
              nickName: "The A Team",
              start: {
                day: 1,
                month: 1,
                year: 2020,
              },
              students: new Set(["student123"]),
            },
          ],
        },
      ])
    );
  });

  it("rejects request if section does not exist", async () => {
    const repo = toInMemoryRepo([toFullStackDevCourseWithEmptySection()]);
    expect(await register(repo, "student123", "bad_section_id")()).toEqual(
      E.left(toFailure("Could not find course with provided section id"))
    );
    expect(await repo.fetchAll()()).toEqual(
      E.right([toFullStackDevCourseWithEmptySection()])
    );
  });

  it("can take the last seat in a section", async () => {
    const repo = toInMemoryRepo([toFullStackDevCourseWithNearlyFullSection()]);
    expect(await register(repo, "student123", "1-1")()).toEqual(
      E.right({
        ...toFullStackDevCourseWithNearlyFullSection(),
        sections: [
          {
            id: "1-1",
            nickName: "The A Team",
            start: {
              day: 1,
              month: 1,
              year: 2020,
            },
            students: new Set([
              "1",
              "2",
              "3",
              "4",
              "5",
              "6",
              "7",
              "8",
              "9",
              "student123",
            ]),
          },
        ],
      })
    );

    expect(await repo.fetchAll()()).toEqual(
      E.right([
        {
          ...toFullStackDevCourseWithNearlyFullSection(),
          sections: [
            {
              id: "1-1",
              nickName: "The A Team",
              start: {
                day: 1,
                month: 1,
                year: 2020,
              },
              students: new Set([
                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7",
                "8",
                "9",
                "student123",
              ]),
            },
          ],
        },
      ])
    );
  });

  it("rejects registration if section is full", async () => {
    const repo = toInMemoryRepo([toFullStackDevCourseWithFullSection()]);
    expect(await register(repo, "student123", "1-1")()).toEqual(
      E.left(toFailure("Section is full"))
    );
    expect(await repo.fetchAll()()).toEqual(
      E.right([toFullStackDevCourseWithFullSection()])
    );
  });

  it("returns error if the repo fails on fetch", async () => {
    const repo = toFailingRepo();
    expect(await register(repo, "student123", "1-1")()).toEqual(
      E.left(toFailure("Unexpected failure when fetching from the db"))
    );
  });

  it("returns error if the repo fails on save", async () => {
    const repo = toInMemoryRepoThatFailsOnUpdate([
      toFullStackDevCourseWithEmptySection(),
    ]);
    expect(await register(repo, "student123", "1-1")()).toEqual(
      E.left(toFailure("Unexpected failure when saving to the db"))
    );
  });
});
