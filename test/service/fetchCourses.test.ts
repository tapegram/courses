import * as TE from "fp-ts/lib/TaskEither";
import * as E from "fp-ts/lib/Either";
import { fetchCourses } from "../../service/main";
import { Course, toFailure } from "../../service/domain";
import { toFailingRepo, toDefaultCourse } from "./shared";

const toFullStackDevCourse = () =>
  toDefaultCourse("1", "Full Stack Dev Course", "NextJS!");
const toBackendDevCourse = () =>
  toDefaultCourse("2", "Backend Dev Course", "TDD, DDD, XP, FP, ...ETC!");

describe("fetchCourses", () => {
  it("returns all available courses", async () => {
    expect(
      await fetchCourses({
        fetchAll: () => TE.right([]),
        update: (course: Course) => TE.left(toFailure("Unimplemented")),
      })()
    ).toEqual(E.right([]));
    expect(
      await fetchCourses({
        fetchAll: () => TE.right([toFullStackDevCourse()]),
        update: (course: Course) => TE.left(toFailure("Unimplemented")),
      })()
    ).toEqual(E.right([toFullStackDevCourse()]));
    expect(
      await fetchCourses({
        fetchAll: () =>
          TE.right([toFullStackDevCourse(), toBackendDevCourse()]),
        update: (course: Course) => TE.left(toFailure("Unimplemented")),
      })()
    ).toEqual(E.right([toFullStackDevCourse(), toBackendDevCourse()]));
  });

  it("returns error if the repo fails on fetch", async () => {
    expect(await fetchCourses(toFailingRepo())()).toEqual(
      E.left(toFailure("Unexpected failure when fetching from the db"))
    );
  });
});
