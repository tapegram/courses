import { left, right } from "fp-ts/lib/TaskEither";
import {
  Course,
  CourseDescription,
  CourseId,
  CourseName,
  CourseRepo,
  Section,
  Session,
  StudentId,
  toFailure,
} from "../../service/domain";

export const toFailingRepo = (): CourseRepo => {
  return {
    fetchAll: () =>
      left(toFailure("Unexpected failure when fetching from the db")),
    update: (course: Course) =>
      left(toFailure("Unexpected failure when saving to the db")),
  };
};

export const toInMemoryRepoThatFailsOnUpdate = (
  initial_data: Course[]
): CourseRepo => {
  let courses: Course[] = initial_data;
  return {
    fetchAll: () => right(courses),
    update: (course: Course) =>
      left(toFailure("Unexpected failure when saving to the db")),
  };
};

export const toDefaultSessions = (id: CourseId) => [
  {
    id: `${id}-1`,
    sessionNumber: "1",
    name: "session 1",
    description: `${id}-2`,
  },
  {
    id: `${id}-2`,
    sessionNumber: "2",
    name: "session 2",
    description: `${id}-2`,
  },
  {
    id: `${id}-3`,
    sessionNumber: "3",
    name: "session 3",
    description: `${id}-3`,
  },
  {
    id: `${id}-4`,
    sessionNumber: "4",
    name: "session 4",
    description: `${id}-4`,
  },
];

export const toDefaultSections = (id: CourseId) => [
  {
    id: `${id}-1`,
    nickName: "The A Team",
    start: {
      day: 1,
      month: 1,
      year: 2020,
    },
    students: new Set<StudentId>(),
  },
  {
    id: `${id}-2`,
    nickName: "The B Team",
    start: {
      day: 15,
      month: 1,
      year: 2020,
    },
    students: new Set<StudentId>(),
  },
];

export const toDefaultCourse = (
  id: CourseId,
  name: CourseName,
  description: CourseDescription
) =>
  toCourse(id, name, description, toDefaultSessions(id), toDefaultSections(id));

export const toCourse = (
  id: CourseId,
  name: CourseName,
  description: CourseDescription,
  sessions: Session[],
  sections: Section[]
) => ({
  id: id,
  name: name,
  description: description,
  sessions: sessions,
  sections: sections,
});

export const toFullSection = (id: CourseId): Section => ({
  id: `${id}-1`,
  nickName: "The A Team",
  start: {
    day: 1,
    month: 1,
    year: 2020,
  },
  students: new Set(["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]),
});

export const toEmptySection = (id: CourseId): Section => ({
  id: `${id}-1`,
  nickName: "The A Team",
  start: {
    day: 1,
    month: 1,
    year: 2020,
  },
  students: new Set<StudentId>(),
});

export const toSectionWithOneSeatOpen = (id: CourseId): Section => ({
  id: `${id}-1`,
  nickName: "The A Team",
  start: {
    day: 1,
    month: 1,
    year: 2020,
  },
  students: new Set(["1", "2", "3", "4", "5", "6", "7", "8", "9"]),
});
