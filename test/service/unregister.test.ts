import * as E from "fp-ts/lib/Either";
import { unregister } from "../../service/main";
import { toFailure } from "../../service/domain";
import { toInMemoryRepo } from "../../data/inMemoryRepo";
import {
  toCourse,
  toFailingRepo,
  toInMemoryRepoThatFailsOnUpdate,
  toDefaultSessions,
} from "./shared";

const toFullStackDevCourseWithStudentInSecondSection = () =>
  toCourse(
    "1",
    "Full Stack Dev",
    "Learn Full Stack Dev!",
    toDefaultSessions("1"),
    [
      {
        id: "1-1",
        nickName: "The A Team",
        start: {
          day: 1,
          month: 1,
          year: 2020,
        },
        students: new Set(),
      },
      {
        id: "1-2",
        nickName: "The B Team",
        start: {
          day: 15,
          month: 1,
          year: 2020,
        },
        students: new Set(["student123"]),
      },
    ]
  );

describe("unregister", () => {
  it("remove student from section if they are registered", async () => {
    const repo = toInMemoryRepo([
      toFullStackDevCourseWithStudentInSecondSection(),
    ]);
    expect(await unregister(repo, "student123", "1-2")()).toEqual(
      E.right({
        ...toFullStackDevCourseWithStudentInSecondSection(),
        sections: [
          {
            id: "1-1",
            nickName: "The A Team",
            start: {
              day: 1,
              month: 1,
              year: 2020,
            },
            students: new Set(),
          },
          {
            id: "1-2",
            nickName: "The B Team",
            start: {
              day: 15,
              month: 1,
              year: 2020,
            },
            students: new Set(),
          },
        ],
      })
    );

    expect(await repo.fetchAll()()).toEqual(
      E.right([
        {
          ...toFullStackDevCourseWithStudentInSecondSection(),
          sections: [
            {
              id: "1-1",
              nickName: "The A Team",
              start: {
                day: 1,
                month: 1,
                year: 2020,
              },
              students: new Set(),
            },
            {
              id: "1-2",
              nickName: "The B Team",
              start: {
                day: 15,
                month: 1,
                year: 2020,
              },
              students: new Set(),
            },
          ],
        },
      ])
    );
  });

  it("does nothing if student is not in section", async () => {
    const repo = toInMemoryRepo([
      toFullStackDevCourseWithStudentInSecondSection(),
    ]);
    expect(await unregister(repo, "student123", "1-1")()).toEqual(
      E.right({
        ...toFullStackDevCourseWithStudentInSecondSection(),
        sections: [
          {
            id: "1-1",
            nickName: "The A Team",
            start: {
              day: 1,
              month: 1,
              year: 2020,
            },
            students: new Set(),
          },
          {
            id: "1-2",
            nickName: "The B Team",
            start: {
              day: 15,
              month: 1,
              year: 2020,
            },
            students: new Set(["student123"]),
          },
        ],
      })
    );

    expect(await repo.fetchAll()()).toEqual(
      E.right([
        {
          ...toFullStackDevCourseWithStudentInSecondSection(),
          sections: [
            {
              id: "1-1",
              nickName: "The A Team",
              start: {
                day: 1,
                month: 1,
                year: 2020,
              },
              students: new Set(),
            },
            {
              id: "1-2",
              nickName: "The B Team",
              start: {
                day: 15,
                month: 1,
                year: 2020,
              },
              students: new Set(["student123"]),
            },
          ],
        },
      ])
    );
  });

  it("rejects request if section does not exist", async () => {
    const repo = toInMemoryRepo([
      toFullStackDevCourseWithStudentInSecondSection(),
    ]);
    expect(await unregister(repo, "student123", "bad_section_id")()).toEqual(
      E.left(toFailure("Could not find course with provided section id"))
    );
    expect(await repo.fetchAll()()).toEqual(
      E.right([toFullStackDevCourseWithStudentInSecondSection()])
    );
  });

  it("returns error if the repo fails on fetch", async () => {
    const repo = toFailingRepo();
    expect(await unregister(repo, "student123", "1-1")()).toEqual(
      E.left(toFailure("Unexpected failure when fetching from the db"))
    );
  });

  it("returns error if the repo fails on save", async () => {
    const repo = toInMemoryRepoThatFailsOnUpdate([
      toFullStackDevCourseWithStudentInSecondSection(),
    ]);
    expect(await unregister(repo, "student123", "1-1")()).toEqual(
      E.left(toFailure("Unexpected failure when saving to the db"))
    );
  });
});
