import { toDefaultCourse, toSession } from "./shared";
import { getSession } from "../../../lib/shared";
import { none, some } from "fp-ts/Option";

describe("getSession", () => {
  it("returns none if no matching session", async () => {
    expect(getSession(toDefaultCourse("1"), "99-99")).toEqual(none);
  });
  it("returns some session if a match exists", async () => {
    expect(getSession(toDefaultCourse("1"), "1-3")).toEqual(
      some(toSession("1-3", "3", "Session 3", "Third Session"))
    );
  });
});
