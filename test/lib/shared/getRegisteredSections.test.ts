import { toDefaultCourse, toSection, toDate } from "./shared";
import { getRegisteredSections } from "../../../lib/shared";

describe("getRegisteredCourses", () => {
  it("returns empty array if no registered sections", async () => {
    expect(getRegisteredSections("99", toDefaultCourse("1"))).toEqual([]);
  });
  it("returns section if single registered section", async () => {
    expect(getRegisteredSections("1", toDefaultCourse("1"))).toEqual([
      toSection("1-1", "The A Team", toDate(1, 1, 2020), ["1", "2", "3"]),
    ]);
  });
  it("returns multiple sections if multiple registrations", async () => {
    expect(getRegisteredSections("3", toDefaultCourse("1"))).toEqual([
      toSection("1-1", "The A Team", toDate(1, 1, 2020), ["1", "2", "3"]),
      toSection("1-2", "The B Team", toDate(15, 1, 2020), ["3", "4", "5", "6"]),
    ]);
  });
});
