import * as T from "../../../lib/type-defs.graphqls";

export const toCourse = (
  id: string,
  name: string,
  description: string,
  sessions: T.Session[],
  sections: T.Section[]
): T.Course => ({
  id,
  name,
  description,
  sessions,
  sections,
});

export const toSection = (
  id: string,
  nickName: string,
  start: T.Date,
  students: string[]
): T.Section => ({
  id,
  nickName,
  start,
  students,
});

export const toSession = (
  id: string,
  sessionNumber: string,
  name: string,
  description: string
): T.Session => ({
  id,
  sessionNumber,
  name,
  description,
});

export const toDate = (day: number, month: number, year: number): T.Date => ({
  day,
  month,
  year,
});

export const toDefaultCourse = (id: string): T.Course =>
  toCourse(
    id,
    "Default Course",
    "You will learn something, probably.",
    [
      toSession(id + "-1", "1", "Session 1", "First Session"),
      toSession(id + "-2", "2", "Session 2", "Second Session"),
      toSession(id + "-3", "3", "Session 3", "Third Session"),
      toSession(id + "-4", "4", "Session 4", "Final Session"),
    ],
    [
      toSection(id + "-1", "The A Team", toDate(1, 1, 2020), ["1", "2", "3"]),
      toSection(id + "-2", "The B Team", toDate(15, 1, 2020), [
        "3",
        "4",
        "5",
        "6",
      ]),
    ]
  );
