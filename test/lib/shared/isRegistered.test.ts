import { toSection, toDate } from "./shared";
import { isRegistered } from "../../../lib/shared";
import * as T from "../../../lib/type-defs.graphqls";

const toSectionWithStudents = (students: string[]): T.Section =>
  toSection("1", "The A Team", toDate(1, 1, 2020), students);

describe("isRegistered", () => {
  it("false if student not in section", async () => {
    expect(isRegistered("1", toSectionWithStudents(["99", "999"]))).toEqual(
      false
    );
  });
  it("true if student in section", async () => {
    expect(
      isRegistered("1", toSectionWithStudents(["99", "1", "999"]))
    ).toEqual(true);
  });
});
