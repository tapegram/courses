import { toDefaultCourse } from "./shared";
import { getCourse } from "../../../lib/shared";
import { none, some } from "fp-ts/Option";

describe("getCourse", () => {
  it("returns none if no courses", async () => {
    expect(getCourse("1", [])).toEqual(none);
  });
  it("returns none if no matching courses", async () => {
    expect(getCourse("1", [toDefaultCourse("2")])).toEqual(none);
  });
  it("returns some course if a match exists", async () => {
    expect(getCourse("1", [toDefaultCourse("1")])).toEqual(
      some(toDefaultCourse("1"))
    );
  });
});
