import { toDefaultCourse } from "./shared";
import { isRegisteredForCourse } from "../../../lib/shared";

describe("getRegisteredCourses", () => {
  it("false if not registered", async () => {
    expect(isRegisteredForCourse("99", toDefaultCourse("1"))).toEqual(false);
  });
  it("true if registered", async () => {
    expect(isRegisteredForCourse("1", toDefaultCourse("1"))).toEqual(true);
    expect(isRegisteredForCourse("5", toDefaultCourse("1"))).toEqual(true);
  });
});
