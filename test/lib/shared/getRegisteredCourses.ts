import { toDefaultCourse } from "./shared";
import { getRegisteredCourses } from "../../../lib/shared";

describe("getRegisteredCourses", () => {
  it("returns empty array if no courses", async () => {
    expect(getRegisteredCourses("1", [])).toEqual([]);
  });
  it("returns empty array if no registered courses", async () => {
    expect(getRegisteredCourses("99", [toDefaultCourse("1")])).toEqual([]);
  });
  it("returns course if single registered course", async () => {
    expect(getRegisteredCourses("1", [toDefaultCourse("1")])).toEqual([
      toDefaultCourse("1"),
    ]);
  });
  it("returns courses if multiple registered courses", async () => {
    expect(
      getRegisteredCourses("1", [toDefaultCourse("1"), toDefaultCourse("2")])
    ).toEqual([toDefaultCourse("1"), toDefaultCourse("2")]);
  });
});
