import { toDefaultCourse } from "./shared";
import { toRegistrations, toRegistration } from "../../../lib/shared";

describe("toRegistrations", () => {
  it("returns empty array if no courses", async () => {
    expect(toRegistrations("1", [])).toEqual([]);
  });
  it("returns empty array if no registrations", async () => {
    expect(
      toRegistrations("99", [toDefaultCourse("1"), toDefaultCourse("2")])
    ).toEqual([]);
  });
  it("find registration in course", async () => {
    const course = toDefaultCourse("1");
    expect(toRegistrations("5", [course])).toEqual([
      toRegistration(course.id, course.name, "1-2", "The B Team"),
    ]);
  });
  it("find multiple registrations in course", async () => {
    const course = toDefaultCourse("1");
    expect(toRegistrations("3", [course])).toEqual([
      toRegistration(course.id, course.name, "1-1", "The A Team"),
      toRegistration(course.id, course.name, "1-2", "The B Team"),
    ]);
  });
  it("find multiple registrations from multiple courses", async () => {
    const course1 = toDefaultCourse("1");
    const course2 = toDefaultCourse("2");
    expect(toRegistrations("1", [course1, course2])).toEqual([
      toRegistration(course1.id, course1.name, "1-1", "The A Team"),
      toRegistration(course2.id, course2.name, "2-1", "The A Team"),
    ]);
  });
});
