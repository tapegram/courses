# Testing!

# Service

I like "component" level testing which puts the tests on the larger application level behaviors instead of the tradtional every single method (similar to only testing public methods, but with more focus on application vs domain layers).

I prefer tests like this because you can really exhaustively test the "behavior" of the system without coupling your tests to your implementation. Testing is super important and probably the most important part of the work anyone does, but "bad" tests can hurt you. In my mind, bad tests are tests that are any of

1. coupled to implementation (testing implementations instead of abstractions, testing private methods, mocking)
2. not being clear what requirement the test is checking for (if the test failed could you actually tell why it was written and what it is trying to ensure? otherwise how are you supposed to fix it?)

So at least in the Service, I load up the majority of my tests on the exposed service methods so that their underlining implementations are easy to change and refactor without updating the tests, and the tests themselves are more focused on business logic/requirements, and not technical concerns.

Of course, sometimes you just wanna write unit tests because they help you make sure something works! That's great just be ok with deleting them and starting over again if you have to change the implementation.

Also note: because we are using a P&A style arch for the backend, we can dependency inject repo implementations into our tests without a framework or mocking or anything, which akes it really easy to test (and faster, since its all in memory!)

# Lib (graphql type utils)

Added a bunch of functions for common behavior on the graphql types (which in this app are also the client types). I don't expect them to change much so I added a bunch of unit tests. They were all pure functions so they were easy to write tests for as well.

# Client

I don't really know the best way to write client side tests so I didn't. Long term, any kind of core and stable components we could probably tie down with some rendering/snapshot tests, but it didn't seem helpful for the size of this project.

# Integration

The big miss here is integration tests for the graphql api backed with a real postgres DB to test all the plumbing (though the types cover a lot of this). I spent a small amount of time trying to figure out how to init apolloclient in a test mode and wire in even just an in memory repo but it was a headache. Most testing blog posts just hard code the server response instead of actually processing the message, so I gave up and moved on, given the time constraints of this work.

Ideally, I would like

1. In memory tests against the graphql api (with in memory repo / db implementations) so we can be sure about all the plumbing
2. End to End tests in a prodlike staging environment (or even better, on a deployed env on the PR before merge) with actual postgres / 3rd parties, etc.
