# Data

Heres a fun little directory dedicated to all things persistence! At the moment, this directory just houses implementations of the `CourseRepo` type: `PostgresCourseRepo` and `InMemoryRepo`.

## Postgres

As a note, getting the postgres implementation to work was p painful. I ended up using `typeorm` because it seemed to have the most documentation but I ran into some problems along the way including

1. The connection pool seems unreliable given the structure of the app. It was constantly trying to recreate the same default connection which was causing errors (and I couldn't find a good "bootstrapping" part of the app to put it in, maybe I need to make that somehow?).
2. When I was able to implement a kind of singleton thing it was still breaking because `getConnection` wasn't preserving the registered orm models, so it wouldn't actually work.

I spent a bit of time on this but decided it wasn't worth it and just updated the `connection.ts` utility to close existing connections and recreate them on demand. Very not good, but I didn't want to spend too much time on that. I would definitely be uncomfortable releasing this implementation into production, and would want to spend more time looking into how to not have to keep recreating the connection.

## In Memory

A little closure that implements the same `CourseRepo` interface but we can hardcode data into. I used this for building most of the site and then swapped in postgres at the end. What I would like to do is set up some environment based configs to allow starting up the app locally with either postgres or in memory, so if you were developing something where you didnt care about if the data was coming from a "real db" or not (which is most of the time) you wouldn't have to have docker running continuously or any of the extra overhead.

## Seed

Theres a seed function here that is really sloppy and just shoves a Course record into the db after dropping all the tables. It seemed like the easiest way to go about it. `typeorm` doesn't support seeding first class and there was an extra library for it but this seemed like the easiest way to go about it.

If I was developing real dev tools I'd probably want a script I could run as part of spinning up a dev environment (and that had a lot more data and was configurable for specific use cases. Maybe even copying over anonymized prod data on demand (or from regular snapshots)).
