import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import Course from "./course";

@Entity()
class Session {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("integer")
  sessionNumber: number;

  @Column("text")
  name: string;

  @Column("text")
  description: string;

  @ManyToOne((type) => Course, (course) => course.sessions)
  course: Course;
}

export default Session;
