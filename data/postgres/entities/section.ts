import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from "typeorm";
import Course from "./course";
import Registration from "./registration";

@Entity()
class Section {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("text")
  nickName: string;

  // Saw some recommendations online to just save a string
  // Long term we probably want a UTC timestamp
  @Column("text")
  start: string;

  @ManyToOne((type) => Course, (course) => course.sections)
  course: Course;

  @OneToMany((type) => Registration, (registration) => registration.section, {
    cascade: true,
  })
  registrations: Registration[];
}

export default Section;
