import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import Section from "./section";

@Entity()
class Registration {
  @PrimaryGeneratedColumn()
  id: number;

  // Not creating an entity for students in this exercise
  // But definitely need it in real life!
  @Column("text")
  studentId: string;

  @ManyToOne((type) => Section, (section) => section.registrations)
  section: Section;
}

export default Registration;
