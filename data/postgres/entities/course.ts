import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from "typeorm";

import Section from "./section";
import Session from "./session";

@Entity()
class Course {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("text")
  name: string;

  @Column("text")
  description: string;

  @OneToMany((type) => Section, (section) => section.course, { cascade: true })
  sections: Section[];

  @OneToMany((type) => Session, (session) => session.course, { cascade: true })
  sessions: Session[];
}

export default Course;
