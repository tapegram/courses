import "reflect-metadata";
import { createConnection } from "typeorm";

import Course from "./entities/course";
import Registration from "./entities/registration";
import Section from "./entities/section";
import Session from "./entities/session";
import { getConnectionManager, getConnection, Connection } from "typeorm";

/*
next doesn't provide a great 1 time bootstrapping location,
so I'm attempting to cobble together a check to make sure we don't 
create the same `default` connection multiple times (and cause an error)


additionally, even when retrieving an existing connection, it appears to no longer have
the right entities registered and it breaks. So as a hack until I figure out how TypeORM works with
NextJS  I'm gonna just reconnect the connection on demand.

:(
*/
const getOrCreatePostgresConnection = async (): Promise<Connection> => {
  try {
    const connection = getConnectionManager().get();
    await connection.close();
    return await createConnection({
      type: "postgres",
      host: "localhost",
      port: 5560,
      username: "admin",
      password: "password",
      database: "course-db",
      entities: [Course, Registration, Section, Session],
      synchronize: true,
      logging: false,
    });
  } catch {
    return await createConnection({
      type: "postgres",
      host: "localhost",
      port: 5560,
      username: "admin",
      password: "password",
      database: "course-db",
      entities: [Course, Registration, Section, Session],
      synchronize: true,
      logging: false,
    });
  }
};

export default getOrCreatePostgresConnection;
