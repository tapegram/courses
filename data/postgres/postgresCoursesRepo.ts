import * as D from "../../service/domain";
import getOrCreatePostgresConnection from "./connection";
import Course from "./entities/course";
import Session from "./entities/session";
import Section from "./entities/section";
import Registration from "./entities/registration";
import { toRegistration } from "./seed";
import * as TE from "fp-ts/lib/TaskEither";
import * as E from "fp-ts/lib/Either";
import * as O from "fp-ts/lib/Option";
import { pipe } from "fp-ts/lib/pipeable";
import { Repository } from "typeorm";

const toPostgresCourseRepo = (): D.CourseRepo => {
  return {
    fetchAll: (): TE.TaskEither<D.Failure, D.Course[]> => {
      return pipe(
        TE.tryCatch(
          () => getOrCreatePostgresConnection(),
          (err) => {
            console.log(err);
            return D.toFailure(`${err}`);
          }
        ),
        TE.map((connection) => connection.getRepository(Course)),
        TE.chain((courseRepository) =>
          TE.tryCatch(
            () =>
              courseRepository.find({
                relations: ["sections", "sessions", "sections.registrations"],
              }),
            (err) => {
              console.log(err);
              return D.toFailure(`${err}`);
            }
          )
        ),
        TE.map((courses) => courses.map(toDomainCourse))
      );
    },
    update: (course: D.Course): TE.TaskEither<D.Failure, D.Course> => {
      // For now, the only thing that can change is registrations
      // So that is all I'm going to implement here (and will make assumptions about sections not being added or removed)
      return pipe(
        TE.tryCatch(
          () => getOrCreatePostgresConnection(),
          (err) => {
            console.log(err);
            return D.toFailure(`${err}`);
          }
        ),
        TE.map((connection) => ({
          courseRepository: connection.getRepository(Course),
          registrationRepository: connection.getRepository(Registration),
        })),
        TE.chain((repos) =>
          TE.tryCatch(
            () =>
              updateCourse(
                course,
                repos.courseRepository,
                repos.registrationRepository
              ),
            (err) => {
              console.log(err);
              return D.toFailure(`${err}`);
            }
          )
        ),
        TE.map(() => course)
      );
    },
  };
};

export default toPostgresCourseRepo;

const updateCourse = async (
  course: D.Course,
  courseRepository: Repository<Course>,
  registrationRepository: Repository<Registration>
) => {
  // WIP: refactor this whole method to use Task/TaskEither
  const courseEntities = await courseRepository.findByIds([course.id], {
    relations: ["sections", "sections.registrations", "sessions"],
  });

  if (courseEntities.length != 1) {
    throw new Error("Could not find course to update");
  }

  const courseEntity = courseEntities[0];

  courseEntity.sections.map((section) => {
    // Don't orphan registration records
    // It would be nice if the ORM did this by itself.
    // https://github.com/typeorm/typeorm/issues/1351
    section.registrations.map((registration) =>
      registrationRepository.remove(registration)
    );
    section.registrations = pipe(
      D.findSection(course, section.id.toString()),
      O.map(toRegistrationsFromSection),
      O.fold(
        () => [], // Since for now we only update registrations, I'm gonna pretend this cant happen
        (registrations) => registrations
      )
    );
  });

  return await courseRepository.save(courseEntity);
};

const toRegistrationsFromSection = (section: D.Section): Registration[] =>
  [...section.students].map(toRegistration);

const findCourseById = (
  id: string,
  courseRepository: Repository<Course>
): TE.TaskEither<D.Failure, Course> =>
  pipe(
    TE.tryCatch(
      () =>
        courseRepository.findByIds([id], {
          relations: ["sections", "sections.registrations", "sessions"],
        }),
      (err) => {
        console.log(err);
        return D.toFailure(`${err}`);
      }
    ),
    TE.chain((courses) => TE.fromEither(toOnlyCourseOrFailure(courses)))
  );

const toOnlyCourseOrFailure = (
  courses: Course[]
): E.Either<D.Failure, Course> =>
  courses.length == 1
    ? E.right(courses[0])
    : E.left(D.toFailure("Could not find course to update"));

const toDomainCourse = (course: Course): D.Course => ({
  id: course.id.toString(),
  name: course.name,
  description: course.description,
  sessions: course.sessions.map(toDomainSession),
  sections: course.sections.map(toDomainSection),
});

const toDomainSession = (session: Session): D.Session => ({
  id: session.id.toString(),
  sessionNumber: session.sessionNumber.toString(),
  name: session.name,
  description: session.description,
});

const toDomainSection = (section: Section): D.Section => ({
  id: section.id.toString(),
  nickName: section.nickName,
  start: toDomainDate(section.start),
  students: toDomainStudents(section.registrations),
});

const toDomainDate = (date: string) => {
  const vals = date.split("-");
  return {
    day: +vals[1],
    month: +vals[0],
    year: +vals[2],
  };
};

const toDomainStudents = (registrations: Registration[]) =>
  new Set(registrations.map((registration) => registration.studentId));
