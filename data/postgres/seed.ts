import "reflect-metadata";
import Course from "./entities/course";
import Section from "./entities/section";
import Session from "./entities/session";
import Registration from "./entities/registration";
import getOrCreatePostgresConnection from "./connection";

const seed = async () => {
  const connection = await getOrCreatePostgresConnection();

  const courseRepository = connection.getRepository(Course);

  // Drop old data
  // Very unsafe if we ever were considering deploying this code!
  await connection.dropDatabase();
  await connection.synchronize();

  const fullstackDevCourse = toCourse(
    "Fullstack Dev",
    "Fun for the whole team!",
    [
      toSection("The A Team", "01-01-20", [
        toRegistration("1"),
        toRegistration("2"),
        toRegistration("3"),
        toRegistration("4"),
        toRegistration("5"),
        toRegistration("6"),
        toRegistration("7"),
        toRegistration("8"),
        toRegistration("9"),
      ]),
      toSection("The Pretty Good Team", "01-15-20", [
        toRegistration("10"),
        toRegistration("11"),
        toRegistration("12"),
      ]),
    ],
    [
      toSession(1, "Javascript", "Learn Javascript!"),
      toSession(2, "Typescript", "Learn Learn Typescript!"),
      toSession(3, "React", "Learn React!"),
      toSession(
        4,
        "FP",
        "Learn FP and forget about everything else and never be hired again"
      ),
    ]
  );
  await courseRepository.save(fullstackDevCourse);

  // Always report a success. It's a lie but not sure how robust seed data needs to be.
  return {};
};
export default seed;

const toCourse = (
  name: string,
  description: string,
  sections: Section[],
  sessions: Session[]
): Course => {
  const course = new Course();
  course.name = name;
  course.description = description;
  course.sections = sections;
  course.sessions = sessions;
  return course;
};

export const toSection = (
  nickName: string,
  start: string,
  registrations: Registration[]
) => {
  const section = new Section();
  section.nickName = nickName;
  section.start = start;
  section.registrations = registrations;
  return section;
};

const toSession = (
  sessionNumber: number,
  name: string,
  description: string
) => {
  const session = new Session();
  session.sessionNumber = sessionNumber;
  session.name = name;
  session.description = description;
  return session;
};

export const toRegistration = (studentId: string) => {
  const registration = new Registration();
  registration.studentId = studentId;
  return registration;
};
