import { right } from "fp-ts/lib/TaskEither";
import { CourseRepo, Course } from "../service/domain";

export const toInMemoryRepo = (initial_data: Course[]): CourseRepo => {
  let courses: Course[] = initial_data;
  return {
    fetchAll: () => right(courses),
    update: (course: Course) => {
      courses = courses.filter((it) => it.id != course.id);
      courses.push(course);
      return right(course);
    },
  };
};

export const hardCodedInitialData: Course[] = [
  {
    id: "1",
    name: "Learn React",
    description: "Learn it faster",
    sessions: [
      {
        id: "1",
        sessionNumber: "1",
        name: "session 1",
        description: "1-1",
      },
      {
        id: "2",
        sessionNumber: "2",
        name: "session 2",
        description: "1-2",
      },
      {
        id: "3",
        sessionNumber: "3",
        name: "session 3",
        description: "1-3",
      },
      {
        id: "4",
        sessionNumber: "4",
        name: "session 4",
        description: "1-4",
      },
    ],
    sections: [
      {
        id: "1",
        nickName: "The A Team",
        start: {
          day: 1,
          month: 1,
          year: 2020,
        },
        students: new Set(),
      },
      {
        id: "2",
        nickName: "The B Team (Full)",
        start: {
          day: 15,
          month: 1,
          year: 2020,
        },
        students: new Set(["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]),
      },
    ],
  },
  {
    id: "2",
    name: "Learn React Better",
    description: "Learn it better AND faster",
    sessions: [
      {
        id: "5",
        sessionNumber: "1",
        name: "session 1",
        description: "2-1",
      },
      {
        id: "6",
        sessionNumber: "2",
        name: "session 2",
        description: "2-2",
      },
    ],
    sections: [],
  },
];
