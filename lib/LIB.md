# LIB

Not a great name for a directory but it came as part of the original bootstrapping from create-next-project and its where all the graphql types and code generation was put.

I'm using this directory to handle all of the graphql code generation plus holding all of the shared behavior for interacting with the schema types.

Because I decided to use the graphql types directly as the client side types as well, it means these types get used a lot on the client.

The notable files here are:

1. `type-defs.graphqls` - the actual gql schema. This was probably the first thing I worked on for this project. I think coming up with the schema first helps to understand the domain / problem and then decouples the backend from the frontend (to some degree). It required some refactoring but it made it through all of development still looking a lot like its original shape.
2. `resolvers.ts` - this is where we actually wire up the gql queries and mutations to our service. It's the "boundary" of our system (from the backend's perspective) so this is where all the messiness and dependency injection happens (really just a `CourseRepo`)
3. `shared.ts` - all of the little functions for interacting with the gql types.
4. `courses.graphql` - used by the `graphql-let` library to auto generate the typed query/mutation code used by the client.
