import * as TE from "fp-ts/lib/TaskEither";
import { pipe } from "fp-ts/lib/pipeable";
import {
  QueryResolvers,
  MutationResolvers,
  RegisterResult,
  UnregisterResult,
} from "./type-defs.graphqls";
import { ResolverContext } from "./apollo";
import * as Service from "../service/main";
import { toInMemoryRepo, hardCodedInitialData } from "../data/inMemoryRepo";
import toPostgresCourseRepo from "../data/postgres/postgresCoursesRepo";
import { Course, Failure, Section } from "../service/domain";
import seed from "../data/postgres/seed";
import * as T from "fp-ts/lib/Task";

// Eventually we can use env variables to decide between in memory for local and tests and postgres for prod
const hardCodedCourseRepo = toInMemoryRepo(hardCodedInitialData);
const postgresCourseRepo = toPostgresCourseRepo();

// Whatever is set here is the repo we use
// Maybe we can condidationally set this based on env variables
const courseRepo = postgresCourseRepo;

const Query: Required<QueryResolvers<ResolverContext>> = {
  fetchCourses: async (_parent, _args, _context, _info) =>
    await pipe(
      Service.fetchCourses(courseRepo),
      TE.getOrElse<Failure, Course[]>(() => T.of([])),
      T.map((courses) => courses.map(toGraphqlCourse))
    )(),
};

const Mutation: Required<MutationResolvers<ResolverContext>> = {
  register: (_parents, _args, _context, _info) =>
    pipe(
      Service.register(courseRepo, _args.studentId, _args.sectionId),
      TE.fold(
        (e: Failure) => T.of(toRegisterResultFailure(e)),
        (course: Course) => T.of(toRegisterResultSuccess(course))
      )
    )(),
  unregister: (_parents, _args, _context, _info) =>
    pipe(
      Service.unregister(courseRepo, _args.studentId, _args.sectionId),
      TE.fold(
        (e: Failure) => T.of(toUnregisterResultFailure(e)),
        (course: Course) => T.of(toUnregisterResultSuccess(course))
      )
    )(),
  seed: async (_parents, _args, _context, _info) => await seed(),
};

const toRegisterResultFailure = (failure: Failure): RegisterResult => {
  return {
    error: failure.msg,
    course: null,
  };
};

const toRegisterResultSuccess = (course: Course): RegisterResult => {
  return {
    course: toGraphqlCourse(course),
    error: null,
  };
};
const toUnregisterResultFailure = (failure: Failure): UnregisterResult => {
  return {
    error: failure.msg,
    course: null,
  };
};

const toUnregisterResultSuccess = (course: Course): UnregisterResult => {
  return {
    course: toGraphqlCourse(course),
    error: null,
  };
};

const toGraphqlCourse = (course: Course) => ({
  ...course,
  sections: course.sections.map(toGraphqlSection),
});

const toGraphqlSection = (section: Section) => ({
  ...section,
  students: [...section.students],
});

export default { Query, Mutation };
