import { pipe } from "fp-ts/lib/pipeable";
import { Course, Section, Session } from "./type-defs.graphqls";
import { Option, fromNullable } from "fp-ts/Option";

export const getCourse = (
  courseId: string,
  courses: Course[]
): Option<Course> =>
  pipe(
    courses.find((course) => course.id == courseId),
    fromNullable
  );

export const isRegistered = (studentId: string, section: Section): Boolean =>
  !!section.students.find((id) => id == studentId);

export const getSession = (
  course: Course,
  sessionId: string
): Option<Session> =>
  pipe(
    course.sessions.find((session) => session.id == sessionId),
    fromNullable
  );

export const getSection = (
  course: Course,
  sectionId: string
): Option<Section> =>
  pipe(
    course.sections.find((section) => section.id == sectionId),
    fromNullable
  );

export const getRegisteredCourses = (
  studentId: string,
  courses: Course[]
): Course[] =>
  courses.filter((course) => isRegisteredForCourse(studentId, course));

export const getRegisteredSections = (
  studentId: string,
  course: Course
): Section[] =>
  course.sections.filter((section) => isRegistered(studentId, section));

export const isRegisteredForCourse = (
  studentId: string,
  course: Course
): Boolean => getRegisteredSections(studentId, course).length > 0;

export type Registration = {
  courseId: string;
  courseName: string;
  sectionId: string;
  sectionName: string;
};

export const toRegistration = (
  courseId: string,
  courseName: string,
  sectionId: string,
  sectionName: string
): Registration => ({
  courseId: courseId,
  courseName: courseName,
  sectionId: sectionId,
  sectionName: sectionName,
});

export const toRegistrations = (
  studentId: string,
  courses: Course[]
): Registration[] =>
  pipe(
    getRegisteredCourses(studentId, courses),
    (registeredCourses) =>
      registeredCourses.map((course) =>
        pipe(getRegisteredSections(studentId, course), (sections) =>
          sections.map((section) =>
            toRegistration(course.id, course.name, section.id, section.nickName)
          )
        )
      ),
    (registrations) => registrations.flat()
  );
