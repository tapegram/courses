type Props = {
  message: string;
};

const Error = (props: Props) => (
  <>
    <h1>Error</h1>
    <p>{props.message}</p>
  </>
);

export default Error;
