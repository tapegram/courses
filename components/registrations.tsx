import Link from "next/link";
import { Registration } from "../lib/shared";

type RegistrationProps = {
  registrations: Registration[];
};

const Registrations = (props: RegistrationProps) => (
  <>
    <h2>Registered Courses</h2>
    <table>
      <thead>
        <tr>
          <th>Course</th>
          <th>Section Name</th>
        </tr>
      </thead>
      <tbody>
        {props.registrations.map((registration) => (
          <tr key={registration.sectionId}>
            <td>
              <Link href={`/courses/${registration.courseId}`}>
                <a>{`${registration.courseName}`}</a>
              </Link>
            </td>
            <td>{registration.sectionName}</td>
          </tr>
        ))}
      </tbody>
    </table>
  </>
);

export default Registrations;
