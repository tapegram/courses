import NavBar from "./navbar";
import { Container } from "@material-ui/core";

type Props = {
  children: React.ReactNode;
};

const Layout = (props: Props) => (
  <Container>
    <NavBar />
    {props.children}
  </Container>
);

export default Layout;
