import { useRouter } from "next/router";

type Props = {
  isAuthed: Boolean;
  children: React.ReactNode;
};

const LOGIN_PATH = "/login?fromRedirect=true";

const Authed = (props: Props) => {
  const router = useRouter();
  !props.isAuthed ? router.push(LOGIN_PATH) : {};
  return <>{props.children}</>;
};

export default Authed;
