import { Registration } from "../lib/shared";
import Registrations from "./registrations";

type StudentProfileProps = {
  studentId: string;
  registrations: Registration[];
};

const StudentProfile = (props: StudentProfileProps) => {
  return (
    <>
      <h1>Student: {props.studentId}</h1>
      <Registrations registrations={props.registrations} />
    </>
  );
};

export default StudentProfile;
