import * as T from "../lib/type-defs.graphqls";
import Link from "next/link";

type Props = {
  courses: T.Course[];
};
const Courses = (props: Props) => {
  return (
    <div>
      <h1>Courses</h1>
      <ol>
        {props.courses.map((course) => (
          <li key={course.id}>
            <Link href={`/courses/${course.id}`}>
              <a>{`${course.name} - ${course.description}`}</a>
            </Link>
          </li>
        ))}
      </ol>
    </div>
  );
};

export default Courses;
