import Link from "next/link";
import { AppBar, Toolbar, Typography, Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  title: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
}));

const NavBar = () => {
  const classes = useStyles();
  return (
    <div className={classes.grow}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            Courses
          </Typography>
          <div className={classes.grow} />
          <Link href={`/courses/`} passHref>
            <Button color="inherit">Catalog</Button>
          </Link>
          <Link href={`/me`} passHref>
            <Button color="inherit">Me</Button>
          </Link>
          <Link href={`/login`} passHref>
            <Button color="inherit">Login</Button>
          </Link>
        </Toolbar>
      </AppBar>
    </div>
  );
};
export default NavBar;
