# Components

Not too much to say here. I'm still learning some of the best practices for splitting up components. I tried to give them all their own files.

## Authed

Created this component to auto redirect if the user is not logged in. Thought it was nifty to do a component but it is probably better to do it via the next framework's server side rendering, so we can avoid the round trips.
