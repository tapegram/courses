import { setCookie } from "nookies";
import { useState } from "react";

const thirty_days = 30 * 24 * 60 * 60;

type Props = {
  studentId: string | null;
  fromRedirect: Boolean;
};

const Login = (props: Props) => {
  const [studentId, setStudentId] = useState(props.studentId);
  const [newStudentId, setNewStudentId] = useState(props.studentId);

  const handleClick = () => {
    if (!!newStudentId) {
      setCookie(null, "course_student_id", newStudentId, {
        maxAge: thirty_days,
        path: "/",
      });
      setStudentId(newStudentId);
    }
  };

  return (
    <>
      {props.fromRedirect ? <p>Please log in first!</p> : <></>}
      <span>Currently logged in as: {studentId}</span>
      <div>
        <input
          type="text"
          placeholder="new student id"
          onChange={(e) => setNewStudentId(e.target.value)}
        />
        <input type="button" value="change" onClick={handleClick} />
      </div>
    </>
  );
};

export default Login;
