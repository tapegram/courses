import { useUnregisterMutation } from "../lib/courses.graphql";
import { Section as TSection } from "../lib/type-defs.graphqls";

type Props = {
  sectionId: string;
  studentId: string;
  setSections: (sections: TSection[]) => void;
  setError: (message: string) => void;
};

const UnregisterButton = (props: Props) => {
  const [unregisterMutation] = useUnregisterMutation({
    variables: {
      studentId: props.studentId,
      sectionId: props.sectionId,
    },
  });

  const onClickUnregister = async () => {
    const { data, error } = await unregisterMutation();
    if (data && data.unregister.course) {
      props.setSections(data.unregister.course!.sections);
    }
    if (data && data.unregister.error) {
      props.setError(data.unregister.error!);
    } else {
      props.setError("");
    }
  };

  return (
    <input
      type="button"
      value="Unregister"
      onClick={() => onClickUnregister()}
    />
  );
};

export default UnregisterButton;
