import * as T from "../lib/type-defs.graphqls";

type Props = {
  session: T.Session;
};

const Session = (props: Props) => (
  <>
    <p>
      You should only be able to see this page if you are enrolled in the
      course!
    </p>
    <span>Id: {props.session.id}</span>
    <br />
    <span>Session Number: {props.session.sessionNumber}</span>
    <br />
    <span>Name: {props.session.name}</span>
    <br />
    <span>Description: {props.session.description}</span>
    <br />
    <span>Content: This is where it would go if I had any! </span>
  </>
);

export default Session;
