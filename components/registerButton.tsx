import { useRegisterMutation } from "../lib/courses.graphql";
import { Section as TSection } from "../lib/type-defs.graphqls";

type Props = {
  sectionId: string;
  studentId: string;
  setSections: (sections: TSection[]) => void;
  setError: (message: string) => void;
};

const RegisterButton = (props: Props) => {
  const [registerMutation] = useRegisterMutation({
    variables: {
      studentId: props.studentId,
      sectionId: props.sectionId,
    },
  });

  const onClickRegister = async () => {
    const { data } = await registerMutation();
    if (data && data.register.course) {
      props.setSections(data.register.course!.sections);
    }
    if (data && data.register.error) {
      props.setError(data.register.error!);
    } else {
      props.setError("");
    }
  };

  return (
    <input type="button" value="Register" onClick={() => onClickRegister()} />
  );
};

export default RegisterButton;
