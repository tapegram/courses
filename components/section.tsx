import { isRegistered } from "../lib/shared";
import { Section as TSection } from "../lib/type-defs.graphqls";
import UnregisterButton from "./unregisterButton";
import RegisterButton from "./registerButton";

type Props = {
  section: TSection;
  studentId: string;
  setSections: (sections: TSection[]) => void;
  setError: (message: string) => void;
};

const Section = (props: Props) => (
  <>
    <tr key={props.section.id}>
      <td>
        {props.section.start.month}/{props.section.start.day}/
        {props.section.start.year}
      </td>
      <td>{props.section.nickName}</td>
      <td>{JSON.stringify(props.section.students)}</td>
      <td>
        {isRegistered(props.studentId, props.section) ? (
          <UnregisterButton
            studentId={props.studentId}
            sectionId={props.section.id}
            setSections={props.setSections}
            setError={props.setError}
          />
        ) : (
          <RegisterButton
            studentId={props.studentId}
            sectionId={props.section.id}
            setSections={props.setSections}
            setError={props.setError}
          />
        )}
      </td>
    </tr>
  </>
);

export default Section;
