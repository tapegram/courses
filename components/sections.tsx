import { Section as TSection } from "../lib/type-defs.graphqls";
import { useState } from "react";
import Error from "./error";
import Section from "./section";

type Props = {
  sections: TSection[];
  studentId: string;
};

const Sections = (props: Props) => {
  const [sections, setSections] = useState(props.sections);
  const [error, setError] = useState("");
  return (
    <>
      {!!error ? <Error message={error} /> : <></>}
      <h2>Sections</h2>
      <table>
        <thead>
          <tr>
            <th>Start Date</th>
            <th>Section Name</th>
            <th>Students</th>
            <th>Register</th>
          </tr>
        </thead>
        <tbody>
          {sections.map((section) => (
            <Section
              key={section.id}
              section={section}
              studentId={props.studentId}
              setSections={setSections}
              setError={setError}
            />
          ))}
        </tbody>
      </table>
    </>
  );
};

export default Sections;
