import { Course as TCourse } from "../lib/type-defs.graphqls";
import Sections from "./sections";
import Sessions from "./sessions";

type Props = {
  course: TCourse;
  studentId: string;
};

const Course = (props: Props) => (
  <div>
    <h1>{props.course.name}</h1>
    <Sections sections={props.course.sections} studentId={props.studentId} />
    <Sessions sessions={props.course.sessions} courseId={props.course.id} />
  </div>
);

export default Course;
