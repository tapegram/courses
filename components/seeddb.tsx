import { useSeedMutation } from "../lib/courses.graphql";
import { useState } from "react";
import Error from "./error";

type Props = {};

const SeedDB = (props: Props) => {
  const [seedMutation] = useSeedMutation();
  const [error, setError] = useState("");
  const [seeded, setSeeded] = useState(false);

  const onClickSeed = async () => {
    const { data } = await seedMutation();
    if (data?.seed.error) {
      setError(data!.seed.error!);
      setSeeded(false);
    } else {
      setError("");
      setSeeded(true);
    }
  };
  return (
    <>
      <p>Seed the DB with default data (and drop any existing data!)</p>
      {error ? <Error message={error} /> : <></>}
      {seeded ? <p>Seeded Successfully!</p> : <></>}
      <input type="button" value="Seed!" onClick={() => onClickSeed()} />
    </>
  );
};

export default SeedDB;
