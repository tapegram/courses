import Link from "next/link";
import { Session } from "../lib/type-defs.graphqls";

type Props = {
  courseId: string;
  sessions: Session[];
};

const Sessions = (props: Props) => (
  <div>
    <h2>Sessions</h2>
    <table>
      <tbody>
        <tr>
          <th>Name</th>
          <th>Description</th>
        </tr>
        {props.sessions
          .slice()
          .sort((a, b) => +a.sessionNumber - +b.sessionNumber)
          .map((session) => (
            <tr key={session.id}>
              <td>
                <Link
                  href={`/courses/${props.courseId}/sessions/${session.id}`}
                >
                  {session.name}
                </Link>
              </td>
              <td>{session.description}</td>
            </tr>
          ))}
      </tbody>
    </table>
  </div>
);

export default Sessions;
